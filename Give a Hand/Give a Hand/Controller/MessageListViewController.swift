//
//  MessageListViewController.swift
//  Give a Hand
//
//  Created by JIS on 3/2/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit
import Alamofire

class MessageListViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblUserList: UITableView!
    var _messageUserList = [MessageUserEntity]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblUserList.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        
        self.getMsgUsers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getMsgUsers() {
        
        showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
  
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
        },
            to: Constants.REQ_GETMESSAGEUSERS,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("message users :", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                self._messageUserList.removeAll()
                                
                                let messageusers = JSON[Constants.RES_RESMESSAGEUSERS] as! [NSDictionary]
                                
                                for index in 0 ..< messageusers.count {
                                    
                                    let msguser = MessageUserEntity()
                                    msguser._id = Int(messageusers[index][Constants.RES_ID] as! String)!
                                    msguser._username = messageusers[index][Constants.RES_USERNAME] as! String
                                    msguser._photoUrl = messageusers[index][Constants.RES_PHOTOURL] as! String
                                    msguser._message = messageusers[index][Constants.RES_MESSAGE] as! String
                                    msguser._time = messageusers[index][Constants.RES_TIME] as! String
                                    msguser._type = messageusers[index][Constants.RES_TYPE] as! String
                                    
                                    self._messageUserList.append(msguser)
                                    
                                }
                                
                                self.tblUserList.reloadData()
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return _messageUserList.count;
//        return 5
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell") as! UserCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let user = _messageUserList[indexPath.row]
        cell.lblName.text = user._username
        cell.lblLastMsg.text = user._message
        cell.imvPhoto.sd_setImage(with: URL(string:user._photoUrl), placeholderImage: UIImage(named: "img_user"))
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = _messageUserList[indexPath.row]
        let msgVC = self.storyboard?.instantiateViewController(withIdentifier: "MessagingViewController") as! MessagingViewController
        msgVC._target_id = user._id
        msgVC._target_photo = user._photoUrl
        
        self.navigationController?.pushViewController(msgVC, animated: true)
    }
}
