//
//  DetailViewController.swift
//  Give a Hand
//
//  Created by JIS on 3/2/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit
import Cosmos
import KMPlaceholderTextView
import SDWebImage

class DetailViewController: UIViewController {
    
    @IBOutlet weak var btnAttach: UIButton!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var tvDescription: KMPlaceholderTextView!
    
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var imvPicture: UIImageView!
    @IBOutlet weak var imvOwnerPhoto: UIImageView!
    @IBOutlet weak var lblOwnerName: UILabel!
    var selectedItem : ItemEntity!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tabBarController?.tabBar.isHidden = true
        
        tvDescription.layer.borderWidth = 1.0;
        tvDescription.layer.cornerRadius = 3.0;
        tvDescription.layer.borderColor = UIColor.lightGray.cgColor

        btnAttach.layer.borderColor = UIColor.lightGray.cgColor
        btnAttach.layer.cornerRadius = 3.0;
        
        self.initItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    func initItem() {
        
        tfTitle.text = selectedItem._title
        tvDescription.text = selectedItem._description
        imvPicture.sd_setImage(with: URL(string:selectedItem._photoUrl), placeholderImage: UIImage(named: "img_user"))
        imvOwnerPhoto.sd_setImage(with: URL(string:selectedItem._owner._profileUrl), placeholderImage:UIImage(named:"img_user"))
        lblOwnerName.text = selectedItem._owner._username
        
    }
    
    @IBAction func gotoMessage(_ sender: Any) {
        
        var newControllers = [UIViewController]()
        newControllers += self.navigationController!.viewControllers
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let msgVC = storyboard.instantiateViewController(withIdentifier: "MessagingViewController") as! MessagingViewController
        
        msgVC.hidesBottomBarWhenPushed = true;
        
        newControllers[newControllers.count - 1] = msgVC
        
        msgVC._target_id = selectedItem._owner._id
        msgVC._target_photo = selectedItem._owner._profileUrl

        
        self.navigationController?.setViewControllers(newControllers, animated: true)

    }
}
