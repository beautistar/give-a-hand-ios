//
//  LoginViewController.swift
//  Give a Hand
//
//  Created by JIS on 3/2/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit
import Material
import Alamofire

class LoginViewController: BaseViewController {

    @IBOutlet weak var tfUsername: TextField!
    @IBOutlet weak var tfPassword: TextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tapper = UITapGestureRecognizer(target: self, action: #selector(self.handleTapView(_:)))
        self.view.addGestureRecognizer(tapper)
        
        self.initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        let textFields : [TextField] = [tfUsername, tfPassword];
        
        for textField in textFields {
            
            textField.placeholderNormalColor = Color.blue.darken2
            textField.placeholderActiveColor = Color.blue.darken2
            textField.dividerNormalColor = Color.blue.darken2
            textField.dividerActiveColor = Color.blue.darken2
        }
        
        //load from saved data
        let defaults = UserDefaults.standard
       
        let username = defaults.string(forKey: Constants.USERNAME)
        let pwd = defaults.string(forKey: Constants.PASSWORD)
        
        if username?.characters.count != 0 {
            
            tfUsername.text = username
            tfPassword.text = pwd
        }
    }
    
    func handleTapView(_ sender:UITapGestureRecognizer) {
        
        self.view.endEditing(true);
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == tfUsername) {
            tfPassword.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        
        return true
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        if self.checkValid() {
            
            self.doLogin()
        }
    }
    
    func checkValid() -> Bool {
        
        if tfUsername.text == nil || tfUsername.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_USERNAME, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tfPassword.text == nil || tfPassword.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_PWD, positive: R.string.OK, negative: nil)
            return false
        }
        
        return true
    }
    // once sec...
    // bug was come from register screen. then open bug screen
    func doLogin() {
        
        showLoadingView()
        
        let username = tfUsername.text!
        let password = tfPassword.text!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
               
                multipartFormData.append("\(username)".data(using:String.Encoding.utf8)!, withName: "username")
                multipartFormData.append("\(password)".data(using:String.Encoding.utf8)!, withName: "password")
        },
            to: Constants.REQ_LOGIN,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            print("result :", JSON)
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                let user = UserEntity()
                                user._id = Int(JSON[Constants.RES_ID] as! String)!
                                user._username = username
                                user._password = password
                                user._profileUrl = JSON[Constants.RES_PHOTOURL] as! String
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue(user._username, forKey:Constants.USERNAME)
                                defaults.setValue(user._password, forKey: Constants.PASSWORD)
                                
                                AppDelegate.setUser(user: user)
                                
                                self.gotoMain()
                                
                            } else if result_code == 203 {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.WRONG_USER, positive: R.string.OK, negative: nil)
                                
                            } else if result_code == 204 {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.WRONG_PWD, positive: R.string.OK, negative: nil)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
            }
        )
    }
    
    func gotoMain() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainTabVC") as! MainViewController
        
        ((UIApplication.shared.delegate) as! AppDelegate).window?.rootViewController = mainViewController
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
