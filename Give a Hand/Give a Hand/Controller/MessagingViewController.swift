//
//  MessagingViewController.swift
//  Give a Hand
//
//  Created by JIS on 3/3/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit
import Alamofire

class MessagingViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    var _target_id:Int = 0
    var _target_photo:String = ""
    var _messages = [MessageEntity]()
    
    @IBOutlet weak var tvMessage: UITextView!

    @IBOutlet weak var tblMessage: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblMessage.tableFooterView = UIView()
        
        // auto cell - size table view
        tblMessage.estimatedRowHeight = 70
        tblMessage.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.getMessges()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func getMessges() {
        
        showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(self._target_id)".data(using:String.Encoding.utf8)!, withName: "sender_id")
        },
            to: Constants.REQ_GETMESSAGES,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("result :", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                                                       
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                self._messages.removeAll()
                                AppDelegate.getUser()._items.removeAll()
                                
                                let messages = JSON[Constants.RES_RESMESSAGEINFOS] as! [NSDictionary]
                                
                                for index in 0 ..< messages.count {
                                    
                                    let message = MessageEntity()
                                    message._content = messages[index][Constants.RES_CONTENT] as! String
                                    message._type = messages[index][Constants.RES_TYPE] as! String
                                    message._reg_date = messages[index][Constants.RES_REGDATE] as! String
                                    
                                    
                                    self._messages.append(message)
                                }
                                
                                self.tblMessage.reloadData()
                                self.view.endEditing(true)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                }
        }
        )
    }
    
    @IBAction func sendAction(_ sender: Any) {
        
        if tvMessage.text != nil || tvMessage.text?.characters.count != 0 {

            self.sendMessges()
        }
    }
        
    func sendMessges() {
        
        showLoadingView()
        
        let send_message = tvMessage.text!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(self._target_id)".data(using:String.Encoding.utf8)!, withName: "sender_id")
                multipartFormData.append("\(send_message)".data(using:String.Encoding.utf8)!, withName: "message")
        },
            to: Constants.REQ_SENDMESSAGES,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("send message result :", response)
                        
                        self.hideLoadingView()
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                
                                
                                let message = MessageEntity()
                                
                                message._content = self.tvMessage.text
                                message._type = "sent"
                                self._messages.append(message)
                                }
                                
                                self.tblMessage.reloadData()
                            self.view.endEditing(true)
                            self.tvMessage.text = ""
                            
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)                    
                    self.hideLoadingView()
                    
                    return
                }
        }
        )
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        return 5;
        return _messages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 70.0
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message = _messages[indexPath.row]
        
        if message._type == "received" {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiveCell") as! ReceiveCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            
            cell.imvUserPhoto.sd_setImage(with: URL(string:self._target_photo), placeholderImage: UIImage(named: "img_user"))
            cell.lblMessage.text = message._content
            
            return cell
            
        } else /*if message._type == "sent"*/ {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SendCell") as! SendCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            
            cell.imvUserPhoto.sd_setImage(with: URL(string:AppDelegate.getUser()._profileUrl), placeholderImage: UIImage(named: "img_user"))
            cell.lblMessage.text = message._content
            
            return cell            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
}



