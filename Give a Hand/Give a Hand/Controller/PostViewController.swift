//
//  PostViewController.swift
//  Give a Hand
//
//  Created by JIS on 3/2/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit
import MapKit
import Material
import KMPlaceholderTextView
import Alamofire

class PostViewController: BaseViewController, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate {

//    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tvDescription: KMPlaceholderTextView!
    @IBOutlet weak var btnAttach: UIButton!
    @IBOutlet weak var imvPicture: UIImageView!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var rewardSegment: UISegmentedControl?
    
    var category : String = ""
    
    var _imgPic : String?
    
    let _picker: UIImagePickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tabBarController?.tabBar.isHidden = true
        
//        mapView.showsBuildings = true
//        let eiffelTowerCoordinates = CLLocationCoordinate2DMake(48.85815,2.29452)
//        mapView.region = MKCoordinateRegionMakeWithDistance(eiffelTowerCoordinates, 1000,100)
//        
//        mapView.mapType = MKMapType.standard
//        
//        // 3D Camera
//        let mapCamera = MKMapCamera()
//        mapCamera.centerCoordinate = eiffelTowerCoordinates
//        mapCamera.pitch = 45
//        mapCamera.altitude = 500
//        mapCamera.heading = 45
//        
//        // Set MKmapView camera property
//        self.mapView.camera = mapCamera
        
        //////
        tvDescription.layer.borderColor = UIColor.lightGray.cgColor
        tvDescription.layer.borderWidth = 1.0
        
        btnAttach.layer.borderColor = UIColor.lightGray.cgColor
        
        self._picker.delegate = self
        _picker.allowsEditing = true

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func postAction(_ sender: Any) {
        
        if self.checkValid() {
            
            self.doPost()
        }
    }
    
    func doPost() {
        
        showLoadingView()
        var rewards:String = ""
        let title = tfTitle.text!
        let description = tvDescription.text!
        rewards = (rewardSegment?.titleForSegment(at: (rewardSegment?.selectedSegmentIndex)!))!
        
        print(rewards)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(URL(fileURLWithPath:self._imgPic!), withName: "file")
                multipartFormData.append("\(title)".data(using:String.Encoding.utf8)!, withName: "title")
                multipartFormData.append("\(description)".data(using:String.Encoding.utf8)!, withName: "description")
                multipartFormData.append("\(self.category)".data(using:String.Encoding.utf8)!, withName: "category")
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(rewards)".data(using:String.Encoding.utf8)!, withName: "rewards")
        },
            to: Constants.REQ_POST,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                _ = self.navigationController?.popViewController(animated: true)
                                
                            } else if result_code == 205 {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.UPLOAD_FAIL, positive: R.string.OK, negative: nil)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }                        
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
            }
        )
    }

    func checkValid() -> Bool {
        
        if tfTitle.text == nil || tfTitle.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_TITLE, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tvDescription.text == nil || tvDescription.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_DES, positive: R.string.OK, negative: nil)
            return false
        }
        
        if _imgPic == nil {
            
            showAlertDialog(title: nil, message: R.string.SELECT_PIC, positive: R.string.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func attachAction(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let photoSourceAlert: UIAlertController = UIAlertController(title: R.string.PHOTO_SOURCE, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cameraAction: UIAlertAction = UIAlertAction(title: R.string.CAMERA, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(self._picker, animated: true, completion: nil)
            })
            
            let albumAction: UIAlertAction = UIAlertAction(title: R.string.PHOTO_ALBUMS, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            photoSourceAlert.addAction(cameraAction)
            photoSourceAlert.addAction(albumAction)
            photoSourceAlert.addAction(UIAlertAction(title: R.string.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(photoSourceAlert, animated: true, completion: nil);
        }
        else
        {
            _picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            let photoSourceAlert: UIAlertController = UIAlertController(title: R.string.PHOTO_SOURCE, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let albumAction: UIAlertAction = UIAlertAction(title: R.string.PHOTO_ALBUMS, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            
            photoSourceAlert.addAction(albumAction)
            photoSourceAlert.addAction(UIAlertAction(title: R.string.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(photoSourceAlert, animated: true, completion: nil);
            
        }

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            imvPicture.image = pickedImage
            _imgPic = saveToFile(image: pickedImage, filePath: Constants.SAVE_ROOT_PATH, fileName: "temp.png")
        }
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }



}
