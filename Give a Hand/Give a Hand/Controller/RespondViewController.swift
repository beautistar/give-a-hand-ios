//
//  RespondViewController.swift
//  Give a Hand
//
//  Created by JIS on 3/2/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit
import Alamofire
import Material
import SDWebImage

class RespondViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblRespond: UITableView!
    
    var _items = [ItemEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblRespond.tableFooterView = UIView()        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.tabBarController?.tabBar.isHidden = false
        
//        if self._items.count == 0 {
            self.getResponds()
//        }

    }
    
    func getResponds() {
            
        showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                //multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(1)".data(using:String.Encoding.utf8)!, withName: "user_id")
        },
            to: Constants.REQ_GETRESPONDS,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("result :", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            print("result :", JSON)
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                self._items.removeAll()
                                AppDelegate.getUser()._items.removeAll()
                                
                                let items = JSON[Constants.RES_RESPONDS] as! [NSDictionary]
                                
                                for index in 0 ..< items.count {
                                    
                                    let item = ItemEntity()
                                    item._id = Int(items[index][Constants.RES_ID] as! String)!
                                    item._owner._id = Int(items[index][Constants.RES_USERID] as! String)!
                                    item._owner._username = items[index][Constants.RES_USERNAME] as! String
                                    item._owner._profileUrl = items[index][Constants.RES_USERPHOTO] as! String
                                    item._title = items[index][Constants.RES_TITLE] as! String
                                    item._category = items[index][Constants.RES_CATEGORY] as! String
                                    item._description = items[index][Constants.RES_DESCRIPTION] as! String
                                    item._photoUrl = items[index][Constants.RES_PHOTOURL] as! String
                                    item._rewards = items[index][Constants.RES_REWARDS] as! String
                                    item._status = items[index][Constants.RES_STATUS] as! String
                                    
                                    self._items.append(item)
                                    AppDelegate.getUser()._items.append(item)
                                }
                                
                               self.tblRespond.reloadData()
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //return _assigns.count;
        return _items.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell") as! ItemCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let item = _items[indexPath.row]
       
        cell.imvPicture.sd_setImage(with: URL(string:item._photoUrl), placeholderImage: UIImage(named: "img_user"))
        cell.lblTitle.text = item._title
        cell.lblDescription.text = item._description
        cell.lblCategory.text = item._category
        
        return cell
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = _items[indexPath.row]
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "ResEditViewController") as! ResEditViewController
        detailVC.selectedItem = item;
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}
