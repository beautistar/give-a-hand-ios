//
//  ResEditViewController.swift
//  Give a Hand
//
//  Created by JIS on 3/20/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import KMPlaceholderTextView
import Alamofire

class ResEditViewController: BaseViewController {
    
    var selectedItem:ItemEntity!

    @IBOutlet weak var tvDescription: KMPlaceholderTextView!
    @IBOutlet weak var imvPicture: UIImageView!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfStatus: UITextField!
    @IBOutlet weak var lblReward: UILabel!
    
    @IBOutlet weak var btnEdit: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func initUI() {
        
        tvDescription.layer.borderWidth = 1.0;
        tvDescription.layer.cornerRadius = 3.0;
        tvDescription.layer.borderColor = UIColor.lightGray.cgColor
        
        tfTitle.text = selectedItem._title
        tvDescription.text = selectedItem._description
        imvPicture.sd_setImage(with: URL(string:selectedItem._photoUrl), placeholderImage:UIImage(named:"img_user"))
        lblReward.text = selectedItem._rewards
        tfStatus.text = selectedItem._status
        
        if selectedItem._owner._id != AppDelegate.getUser()._id {
            
            self.navigationItem.rightBarButtonItem = nil
        } else {
            self.navigationItem.rightBarButtonItem?.title = "Edit"
        }
    }
    
    @IBAction func editAction(_ sender: Any) {
        
        if tfStatus.text?.characters.count != 0 {
            
            self.updatePost()
        }
    }
    
    func updatePost() {
        
        showLoadingView()
        let _status = tfStatus.text!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(self.selectedItem._id)".data(using:String.Encoding.utf8)!, withName: "id")
                multipartFormData.append("\(_status)".data(using:String.Encoding.utf8)!, withName: "status")
        },
            to: Constants.REQ_UPDATEPOST,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            print("result :", JSON)
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                _ = self.navigationController?.popViewController(animated: true)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                }
        }
        )
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func statusAction(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: Constants.SET_STATUS, rows: Constants.status, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            
            self.tfStatus.text = Constants.status[value]
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
}
