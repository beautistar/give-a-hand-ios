//
//  RegisterViewController.swift
//  Give a Hand
//
//  Created by JIS on 3/2/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit
import Material
import Alamofire


class RegisterViewController: BaseViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var tfUserName: TextField!
    @IBOutlet weak var tfPassword: TextField!
    @IBOutlet weak var imvProfile: UIImageView!
    
    
    let _picker: UIImagePickerController = UIImagePickerController()
    var _imgProfile : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tapper = UITapGestureRecognizer(target: self, action: #selector(self.handleTapView(_:)))
        self.view.addGestureRecognizer(tapper)
        
        self._picker.delegate = self 
        _picker.allowsEditing = true
        
        
        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        let textFields : [TextField] = [tfUserName, tfPassword];
        
        for textField in textFields {
            
            textField.placeholderNormalColor = Color.blue.darken2
            textField.placeholderActiveColor = Color.blue.darken2
            textField.dividerNormalColor = Color.blue.darken2
            textField.dividerActiveColor = Color.blue.darken2
        }
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
        if self.checkValid() {
        
            self.doRegister()
            
        }
    }

    @IBAction func pictureAction(_ sender: Any) {// right
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let photoSourceAlert: UIAlertController = UIAlertController(title: R.string.PHOTO_SOURCE, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cameraAction: UIAlertAction = UIAlertAction(title: R.string.CAMERA, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(self._picker, animated: true, completion: nil)
            })
            
            let albumAction: UIAlertAction = UIAlertAction(title: R.string.PHOTO_ALBUMS, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            photoSourceAlert.addAction(cameraAction)
            photoSourceAlert.addAction(albumAction)
            photoSourceAlert.addAction(UIAlertAction(title: R.string.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(photoSourceAlert, animated: true, completion: nil);
        }
        else
        {
            _picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            let photoSourceAlert: UIAlertController = UIAlertController(title: R.string.PHOTO_SOURCE, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let albumAction: UIAlertAction = UIAlertAction(title: R.string.PHOTO_ALBUMS, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            
            photoSourceAlert.addAction(albumAction)
            photoSourceAlert.addAction(UIAlertAction(title: R.string.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            photoSourceAlert.popoverPresentationController?.sourceRect = imvProfile.bounds /*CGRect(x: imvProfile.superview!.frame.midX,
                                                                                y: imvProfile.superview!.frame.midY,
                                                                                width: imvProfile.frame.size.width * 0.5,
                                                                                height: imvProfile.frame.size.height * 0.5) */
            photoSourceAlert.popoverPresentationController?.sourceView = imvProfile
            
            self.present(photoSourceAlert, animated: true, completion: nil);
            
        }
    }
    
    func handleTapView(_ sender:UITapGestureRecognizer) {
        
        self.view.endEditing(true);
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == tfUserName) {
            tfPassword.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            self.imvProfile.image = pickedImage
            _imgProfile = saveToFile(image: pickedImage, filePath: Constants.SAVE_ROOT_PATH, fileName: "temp.png")
        }
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }


    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    func gotoMain() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainTabVC") as! MainViewController
        
        ((UIApplication.shared.delegate) as! AppDelegate).window?.rootViewController = mainViewController
    }
    
    func checkValid() -> Bool {
        
        if tfUserName.text == nil || tfUserName.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_USERNAME, positive: R.string.OK, negative: nil)
            return false
        }

        if tfPassword.text == nil || tfPassword.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_PWD, positive: R.string.OK, negative: nil)
            return false
        }
        
        if _imgProfile == nil {
            
            showAlertDialog(title: nil, message: R.string.SELECT_PHOTO, positive: R.string.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    func doRegister() {
        
        showLoadingView()
        
        let username = tfUserName.text!
        let password = tfPassword.text!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(URL(fileURLWithPath:self._imgProfile!), withName: "file")
                multipartFormData.append("\(username)".data(using:String.Encoding.utf8)!, withName: "username")
                multipartFormData.append("\(password)".data(using:String.Encoding.utf8)!, withName: "password")
        },
            to: Constants.REQ_REGISTER,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                let user = UserEntity()
                                user._id = JSON[Constants.RES_ID] as! Int
                                user._username = username
                                user._password = password
                                user._profileUrl = JSON[Constants.RES_PHOTOURL] as! String

                                let defaults = UserDefaults.standard
                                defaults.setValue(user._username, forKey:Constants.USERNAME)
                                defaults.setValue(user._password, forKey: Constants.PASSWORD)
                                
                                AppDelegate.setUser(user: user)
                                
                                self.gotoMain()
                                
                            } else if result_code == 201 {
                            
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.USEREMAIL_EXIST, positive: R.string.OK, negative: nil)
                                
                            } else if result_code == 202 {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.UPLOAD_FAIL, positive: R.string.OK, negative: nil)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                        
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
            }
        )
    }

}
