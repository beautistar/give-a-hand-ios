//
//  MessageEntity.swift
//  Give a Hand
//
//  Created by JIS on 3/14/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import Foundation

class MessageUserEntity {
    
    var _id = 0
    var _username = ""
    var _message = ""
    var _photoUrl = ""
    var _type = ""
    var _time = ""    
    
}
