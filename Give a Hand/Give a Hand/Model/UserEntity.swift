//
//  UserEntity.swift
//  Give a Hand
//
//  Created by JIS on 3/3/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import Foundation

class UserEntity {
    
    var _id = 0
    var _username = ""
    var _password = ""   
    var _profileUrl = ""
    var _items = [ItemEntity]()
    
}
