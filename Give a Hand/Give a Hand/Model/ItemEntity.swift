//
//  ItemEntity.swift
//  Give a Hand
//
//  Created by JIS on 3/3/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import Foundation

class ItemEntity {
    
    var _id = 0
    var _owner:UserEntity! = UserEntity()
    var _title = ""
    var _photoUrl = ""
    var _description = ""
    var _category = ""
    var _rewards = ""
    var _status = ""
    
    
}
