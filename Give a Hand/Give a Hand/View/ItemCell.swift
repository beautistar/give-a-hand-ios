//
//  ItemCell.swift
//  Give a Hand
//
//  Created by JIS on 3/3/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {
    @IBOutlet weak var imvPicture: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblCategory: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
