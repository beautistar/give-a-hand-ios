//
//  UserCell.swift
//  Give a Hand
//
//  Created by JIS on 3/3/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLastMsg: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
