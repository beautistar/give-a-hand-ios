//
//  SendCell.swift
//  Give a Hand
//
//  Created by JIS on 3/16/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit

class SendCell: UITableViewCell {

    @IBOutlet weak var imvUserPhoto: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
