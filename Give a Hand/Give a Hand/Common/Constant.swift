//
//  Constant.swift
//  Give a Hand
//
//  Created by JIS on 3/2/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

import UIKit

struct Constants {
    
    
    static let SAVE_ROOT_PATH = "Give_a_Hand"
    
    static let BASE_URL = "http://52.40.98.103/index.php/Api/"
    
    static let REQ_REGISTER = BASE_URL + "register/"
    
    static let REQ_LOGIN = BASE_URL + "login/"
    static let REQ_POST = BASE_URL + "post/"
    static let REQ_UPDATEPOST = BASE_URL + "updatePost/"
    static let REQ_GETRESPONDS = BASE_URL + "getResponds/"
    static let REQ_GETMESSAGEUSERS = BASE_URL + "getMessageUsers/"
    static let REQ_GETMESSAGES = BASE_URL + "getMessageContent/"
    static let REQ_SENDMESSAGES = BASE_URL + "sendMessage/"
    
    static let RES_RESULTCODE = "result_code"
    
    static let RES_ID = "id"
    static let RES_PHOTOURL = "photo_url"
    static let RES_IDX = "idx"
    static let RES_USERID = "user_id"
    static let RES_USERNAME = "user_name"
    static let RES_USERPHOTO = "user_photo"
    static let RES_CATEGORY = "category"
    static let RES_DESCRIPTION = "description"
    static let RES_REWARDS = "rewards"
    static let RES_STATUS = "status"
    static let RES_TITLE = "title"	    
    static let RES_RESPONDS = "responds"
    static let RES_RESMESSAGEUSERS = "message_users"
    static let RES_MESSAGE = "message"
    static let RES_RESMESSAGEINFOS = "message_infos"
    static let RES_TYPE = "type"
    static let RES_TIME = "time"
    static let RES_REGDATE = "reg_date"
    static let RES_CONTENT = "content"
    
    static let USERNAME = "username"
    static let PASSWORD = "password"

    static let SET_STATUS = "Set status"
    static let status = ["Pending", "Ongoing", "Completed", "Cancelled"]
    
    
    static let CODE_SUCESS = 0

}
